<?php

namespace Phplady\CatalogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Phplady\CatalogBundle\Entity\Articul as Articul;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Category
 */
class Category
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var array
     */
    private $articuls;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->articuls = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Add articuls
     *
     * @param Articul $articuls
     * @return Category
     */
    public function addArticul(Articul $articuls)
    {
        $this->articuls[] = $articuls;

        return $this;
    }

    /**
     * Remove articuls
     *
     * @param Articul $articuls
     */
    public function removeArticul(Articul $articuls)
    {
        $this->articuls->removeElement($articuls);
    }

    /**
     * Get articuls
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArticuls()
    {
        return $this->articuls;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
