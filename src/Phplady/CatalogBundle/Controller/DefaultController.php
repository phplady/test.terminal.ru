<?php

namespace Phplady\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="catalog_index")
     * @Route("/{name}", name="catalog_category")
     * @Template()
     */
    public function indexAction($name = null)
    {

        if (!is_null($name)) {
            $repository = $this->getDoctrine()
                ->getRepository('PhpladyCatalogBundle:Category');

            $category = $repository->findOneBy(array('name' => $name));

            if (!$category) {
                throw $this->createNotFoundException('Категория не найдена');
            }
        }

        $repository = $this->getDoctrine()
            ->getRepository('PhpladyCatalogBundle:Articul');

        $items_query = is_null($name) ? $repository->getAllQuery() : $repository->getAllQuery($category->getId());

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $items_query,
            $this->get('request')->query->get('page', 1)/*page number*/,
            5/*limit per page*/
        );

        return array('pagination' => $pagination, 'name' => $name);

    }

    /**
     * @Route("/list/category/{current_path}", name="catalog_list_category")
     * @Template()
     */
    public function categoriesAction(Request $request)
    {
        $current_path = $request->get('current_path');

        $repository = $this->getDoctrine()
            ->getRepository('PhpladyCatalogBundle:Category');

        $items = $repository->getAll();

        return array('items' => $items, 'current_path' => $current_path);

    }
}
