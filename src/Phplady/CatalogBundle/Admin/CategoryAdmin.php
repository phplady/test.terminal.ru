<?php
namespace Phplady\CatalogBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;


class CategoryAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', null, array('required' => true, 
            'label' => 'Название',
            'attr' => array('style' => 'width: 100%;')));
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name', null, array('label' => 'Название'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param Sonata\AdminBundle\Datagrid\DatagridMapper $datagrid
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
    }


} 